class UsersController < ApplicationController
  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      flash[:notice] = "whatever"
    else
      redirect_to new_user_url
    end
  end

  def edit
    @user = User.find(params[:id])
    render :edit
  end

  def reset
    user = User.find_by_email(params[:user][:email])
    if user
      msg = UserMailer.change_password_email(user)
      print msg
      msg.deliver!
    end
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      render :json => "Logged in"
    else
      flash.now[:errors] = @user.errors.full_messages
      redirect_to new_user_url
    end
  end
  
  def show
    render :show
  end
end
