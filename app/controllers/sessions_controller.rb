class SessionsController < ApplicationController

  def new
    @user = User.new
    render :new
  end

  def create
    @user = User.find_by_params(params[:user][:email], params[:user][:password])
    if @user
      login_user!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:notice] = "Do later"
      render :new
    end
  end

  def destroy
    user = User.find(params[:id])
    user.reset_session_token!
    session[:session_token] = nil
    redirect_to new_session_url
  end
end
