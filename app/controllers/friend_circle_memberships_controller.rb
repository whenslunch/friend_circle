# require 'debugger'

class FriendCircleMembershipsController < ApplicationController
  def show
    # @circle = FriendCirc.find(params[:friend_circ_id])
    # @users = User.all
    render :show
  end

  def new
    @circle = FriendCirc.find(params[:friend_circ_id])
    # @user = User.find(params[:user_id])
    @users = User.all
    render :new
  end

  def create
    # debugger
    @user = User.find(params[:user_id])
    @circle = FriendCirc.find(params[:friend_circ_id])
    @circle.member_ids = params[:friend_circle_membership][:user_id]
    # @friend = FriendCircleMembership.new({ :user_id => params[:friend_circle_membership][:user_id],
    #                                        :friend_circ_id => @circle.id })


    if @circle.save
      redirect_to user_friend_circ_url(@user, @circle)
    else
      render :json => "Add member error"
    end
  end
end
