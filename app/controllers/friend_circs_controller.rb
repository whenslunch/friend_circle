class FriendCircsController < ApplicationController
  def new
    @user = User.find(params[:user_id])
    render :new
  end

  def create
    @friend_circ = FriendCirc.new(params[:friend_circ])
    @friend_circ.user_id = current_user.id

    if @friend_circ.save
      # might need to change so user must be logged in user
      redirect_to user_friend_circs_url(current_user)
    else
      flash.now[:errors] = @friend_circ.errors.full_messages
      render :new
    end
  end

  def show
    # @user = User.find(params[:user_id])
    @friend_circ = FriendCirc.find(params[:id])
    render :show
  end
  
  def index
    @user = User.find(params[:user_id])
  end
end
