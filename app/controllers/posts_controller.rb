class PostsController < ApplicationController
  def new
    @user = User.find(params[:user_id])
    render :new
  end
  
  def create
    @post = Post.new({ :body => params[:post][:body]})
    @post.user_id = params[:user_id]
    @post.friend_circ_ids = params[:post][:friend_circ_id]
    
    # params[:link] will return an array of hashes => {:url => "test"}, 
    # params[:link].values will return the individual hashes, not in an array
    @post.links.new(params[:link].values.keep_if { |url_hash| url_hash["url"].length > 0 })
    if @post.save
      render :text => "SUCCESS"
    else
      render :text => "FAIL"
    end
  end
end
