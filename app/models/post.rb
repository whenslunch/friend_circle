class Post < ActiveRecord::Base
  attr_accessible :user_id, :body
  
  belongs_to(
  :user,
  :class_name => "User",
  :foreign_key => :user_id,
  :primary_key => :id
  )
  
  has_many(
  :links,
  :class_name => "Link",
  :foreign_key => :post_id,
  :primary_key => :id,
  :inverse_of => :post
  )
  
  has_many(
  :post_shares,
  :class_name => "PostShare",
  :foreign_key => :post_id,
  :primary_key => :id
  )
  
  has_many :friend_circs, :through => :post_shares, :source => :friend_circ
end
