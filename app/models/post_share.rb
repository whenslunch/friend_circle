class PostShare < ActiveRecord::Base
  attr_accessible :friend_circ_id, :post_id
  
  belongs_to(
  :friend_circ,
  :class_name => "FriendCirc",
  :foreign_key => :friend_circ_id,
  :primary_key => :id
  )
  
  belongs_to(
  :post,
  :class_name => "Post",
  :foreign_key => :post_id,
  :primary_key => :id
  )
end
