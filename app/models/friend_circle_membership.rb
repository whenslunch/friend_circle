class FriendCircleMembership < ActiveRecord::Base
  # attr_accessible :title, :body

  attr_accessible :user_id, :friend_circ_id

  belongs_to(
    :owner,
    :class_name => "User",
    :foreign_key => :user_id,
    :primary_key => :id
  )


  belongs_to(
    :circle,
    :class_name => "FriendCirc",
    :foreign_key => :friend_circ_id,
    :primary_key => :id
  )
end
