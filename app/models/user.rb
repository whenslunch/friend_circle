require 'bcrypt'

class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :password, :session_token
  validates :email, :password_digest, :session_token, presence: true
  before_validation(on: :create) do
    self.reset_session_token! unless self.session_token
  end

  has_many(
    :circles,
    :class_name => "FriendCirc",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  has_many(
    :memberships,
    :class_name => "FriendCircleMembership",
    :foreign_key => :user_id,
    :primary_key => :id
  )

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
    self.save!
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = User.generate_session_token
    self.save!
  end

  def self.find_by_params(email, password)
    user = User.find_by_email(email)
    if user
      return user if user.is_password?(password)
    end
  end
end
