class Link < ActiveRecord::Base
  attr_accessible :url, :post_id
  validates :post, :url, presence: true
  # validates :url, length: { minimum: 1 }
  
  belongs_to(
  :post,
  :class_name => "Post",
  :foreign_key => :post_id,
  :primary_key => :id,
  :inverse_of => :links
  )
  
  # belongs_to(
  # :post,
  # :class_name => "Post",
  # :foreign_key => :post_id,
  # :primary_key => :id
  # )
  
end
