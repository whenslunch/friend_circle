class FriendCirc < ActiveRecord::Base
  attr_accessible :name

  belongs_to(
  :owner,
  :class_name => "User",
  :foreign_key => :user_id,
  :primary_key => :id
  )

  has_many(
  :memberships,
  :class_name => "FriendCircleMembership",
  :foreign_key => :friend_circ_id,
  :primary_key => :id
  )
  
  has_many(
  :post_shares,
  :class_name => "PostShare",
  :foreign_key => :friend_circ_id,
  :primary_key => :id
  )
  
  has_many :posts, :through => :post_shares, :source => :post

  has_many :members, :through => :memberships, :source => :owner


end
