class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def change_password_email(user)
    @user = user
    @url = "http://localhost:3000/users/#{user.id}/edit"
    mail(to: user.email, subject: 'Change Password Email')
  end

end
