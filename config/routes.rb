FriendCircle::Application.routes.draw do
  resource :session
  resources :users do
    resources :friend_circs, shallow: true do
      resources :friend_circle_memberships, shallow: true 
    end
    
    resources :posts, shallow: true do
      resource :links, shallow: true
    end
    
    collection do
      get 'reset'
    end
  end
end
