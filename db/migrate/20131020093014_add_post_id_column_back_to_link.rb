class AddPostIdColumnBackToLink < ActiveRecord::Migration
  def change
    add_column(:links, :post_id, :integer)
    change_column(:links, :post_id, :integer, null: false)
  end
end
