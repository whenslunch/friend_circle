class AddIndexToLink < ActiveRecord::Migration
  def change
    add_index(:links, :post_id)
  end
end
