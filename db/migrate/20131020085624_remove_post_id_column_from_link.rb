class RemovePostIdColumnFromLink < ActiveRecord::Migration
  def change
    remove_column(:links, :post_id)
  end
end
