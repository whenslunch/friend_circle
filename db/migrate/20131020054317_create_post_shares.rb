class CreatePostShares < ActiveRecord::Migration
  def change
    create_table :post_shares do |t|
      t.integer :friend_circ_id, null: false
      t.integer :post_id, null: false
      
      t.timestamps
    end
    
    add_index(:post_shares, :friend_circ_id)
    add_index(:post_shares, :post_id)
  end
end
